﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
public class SaveBoard : MonoBehaviour {


    public static GameBoard Board;

    [MenuItem("Board Actions/ Save Board")]
    private static void Save()
    {
        Board = FindObjectOfType<GameBoard>();
        int[,] Data = new int[Board.Width, Board.Height];
        BoardSave Save = new BoardSave();
        List<int> GoalColors = new List<int>();
        for (int i = 0; i < Board.Width; i++)
        {
            for (int j = 0; j < Board.Height; j++)
            {
                if (Board[i, j] is Mirror)
                {
                    Data[i, j] = 1;
                }
                else if (Board[i, j] is Goal)
                {
                    Data[i, j] = 2;
                    var g = Board[i, j] as Goal;
                    if(g.TargetColor == Color.red)
                    {
                        GoalColors.Add(0);
                    } else if (g.TargetColor == Color.blue)
                    {
                        GoalColors.Add(1);
                    }
                    else if (g.TargetColor == Color.green)
                    {
                        GoalColors.Add(2);
                    }
                    else if (g.TargetColor == Color.magenta)
                    {
                        GoalColors.Add(3);
                    }
                    else if (g.TargetColor == Color.yellow)
                    {
                        GoalColors.Add(4);
                    }
                    else if (g.TargetColor == Color.cyan)
                    {
                        GoalColors.Add(5);
                    }
                    else if (g.TargetColor == Color.white)
                    {
                        GoalColors.Add(6);
                    }
                } else if (Board[i, j] is Wall)
                {
                    Data[i, j] = 3;
                } else if (Board[i,j] is Emitter)
                {
                    Data[i, j] = 4;
                }
                else
                {
                    Data[i, j] = 0;
                }
            }
        }
        var file = new FileStream(EditorUtility.SaveFilePanel("Choose file save location", Application.dataPath + "/Assets/Saves/","NewBoardsave", "boardsave"), FileMode.Create);
        var bf = new BinaryFormatter();
        Save.GoalColors = GoalColors.ToArray();
        Save.SpaceData = Data;
        Save.ColorUses = new int[3];
        Save.ColorUses[0] = Utilities.instance.RedColorButton.Uses;
        Save.ColorUses[1] = Utilities.instance.GreenColorButton.Uses;
        Save.ColorUses[2] = Utilities.instance.BlueColorButton.Uses;
        bf.Serialize(file, Save);
        file.Close();
}
    [MenuItem("Board Actions/ Build New Board")]
    private static void BuildNew()
    {
        Board = FindObjectOfType<GameBoard>();
        var data = new int[Board.Width, Board.Height];
        for(int i = 0; i < Board.Width; i++)
        {
            for(int j = 0; j < Board.Height; j++)
            {
                data[i, j] = 0;
            }
        }
        var save = new BoardSave();
        save.SpaceData = data;
        save.ColorUses = new int[3];
        Board.Init(save);
    }
    [MenuItem("Board Actions/ Load Saved Board")]
    private static void LoadBoard()
    {
        Board = FindObjectOfType<GameBoard>();
        Board.Reset();
        FileStream file = new FileStream(EditorUtility.OpenFilePanel("Select board save" , Application.dataPath +"/Assets/Saves/" , "boardsave"), FileMode.Open);
        Board.Init(new BinaryFormatter().Deserialize(file) as BoardSave);
        file.Close();
    }


    [MenuItem("Board Actions/ Generate Random Board")]
    private static void GenerateRandomBoard()
    {
        FindObjectOfType<GameBoard>().StartCoroutine(GenerateRandom());
    }

    static IEnumerator GenerateRandom()
    {
        Board = FindObjectOfType<GameBoard>();
        int GoalWeight = 1;
        int EmitterWeight = 3;
        int MirrorWeight = 2;
        int WallWeight = 8;
        int EmptyWeight = 50;

        var save = new BoardSave();
        var Solved = false;
        do
        {
            print("Building Board");
            var sum = GoalWeight + EmitterWeight + MirrorWeight + WallWeight + EmptyWeight;
            var data = new int[10, 10];
            int goalCount = 0;
            int emitterCount = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    var val = Random.Range(0, sum);
                    if (val < GoalWeight)
                    {
                        data[j, i] = 2;
                        goalCount++;
                    }
                    else if (val < GoalWeight + EmitterWeight)
                    {
                        data[j, i] = 4;
                        emitterCount++;
                    }
                    else if (val < GoalWeight + EmitterWeight + MirrorWeight)
                    {
                        data[j, i] = 1;
                    }
                    else if (val < GoalWeight + EmitterWeight + MirrorWeight + WallWeight)
                    {
                        data[j, i] = 3;
                    }
                    else
                    {
                        data[j, i] = 0;
                    }
                }
            }
            if (goalCount == 0)
            {
                print("No goals...");
                continue;
            }

            var goalColor = new int[goalCount];
            for (int k = 0; k < goalCount; k++)
            {
                goalColor[k] = Random.Range(0, (int)7);
            }
            var colorCharges = new int[3];
            for (int l = 0; l < 3; l++)
            {
                colorCharges[l] = Random.Range(0, emitterCount);
                emitterCount -= colorCharges[l];
            }
            save.SpaceData = data;
            save.ColorUses = colorCharges;
            save.GoalColors = goalColor;
            yield return CanSolve(save, Solved);
        } while (!Solved);
    }

    private static IEnumerator CanSolve(BoardSave board , bool Solved)
    {
        Dictionary<int, int> EmitterLocations = new Dictionary<int, int>();
        Dictionary<int, int> MirrorLocations = new Dictionary<int, int>();

        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (board.SpaceData[j, i] == 4) EmitterLocations[j] = i;
                else if (board.SpaceData[j, i] == 1) MirrorLocations[j] = i;
            }
        }

        int numMirrors = MirrorLocations.Count;
        int numEmitters = EmitterLocations.Count;
        ConfigValue FirstValue = null;
        ConfigValue LastValue = null;
        ConfigValue PrevValue = null;
        for(int e = 0; e < numEmitters; e++)
        {
            if (e == 0)
            {
                FirstValue = PrevValue = new ConfigValue();
                FirstValue.MaxValue = 11;
                PrevValue.Value = 0;
            } else
            {
                PrevValue.Next = new ConfigValue();
                PrevValue = PrevValue.Next;
                PrevValue.MaxValue = 11;
                PrevValue.Value = 0;
            }
            
        }
        for (int m = 0; m < numMirrors; m++)
        {
            PrevValue.Next = new ConfigValue();
            PrevValue = PrevValue.Next;
            PrevValue.MaxValue = 1;
            PrevValue.Value = 0;
            if(m == numMirrors - 1)
            {
                LastValue = PrevValue;
            }
        }

        Solved = false;
        while (!LastValue.IsDone && !Solved)
        {
            PrintConfig(FirstValue);
            yield return TryConfig(board , FirstValue , Solved);
            FirstValue++;
        }
        
    }


    private static void PrintConfig(ConfigValue head)
    {
        string config = "";
        ConfigValue curent = head;
        while(curent != null)
        {
            config += curent.Value;
            curent = curent.Next;
        }
        print(config);
    }
    private static IEnumerator TryConfig(BoardSave save , ConfigValue head , bool Solved)
    {
        Board.Init(save);
        ConfigValue CurrentValue = head;
        foreach(var e in Board.Emitters)
        {
            int dir = CurrentValue.Value / 3;
            int col = CurrentValue.Value % 3;
            Utilities.Direction direction = 0;
            Color color = Color.black;
            switch (dir)
            {
                case 0:
                    direction = Utilities.Direction.LEFT;
                    break;
                case 1:
                    direction = Utilities.Direction.RIGHT;
                    break;
                case 2:
                    direction = Utilities.Direction.UP;
                    break;
                case 3:
                    direction = Utilities.Direction.DOWN;
                    break;
            }
            switch (col)
            {
                case 0:
                    if (Board.Charges[0] <= 0) continue;
                    color = Color.red;
                    break;
                case 1:
                    if (Board.Charges[1] <= 0) continue;
                    color = Color.green;
                    break;
                case 2:
                    if (Board.Charges[2] <= 0) continue;
                    color = Color.blue;
                    break;
            }
            e.InitialPulses.Add(new ColorPulse(e, direction, color));
            head = head.Next;
        }
        foreach(var m in Board.Mirrors)
        {
            if (head == null) continue;
            if (head.Value == 1) m.Flip();
            head = head.Next;
        }
        yield return new WaitForEndOfFrame();

        Solved = Board.SimulateConfiguration();
        
    }
    class ConfigValue
    {
        public ConfigValue Next;
        public int MaxValue;
        public int Value;
        public bool IsDone = false;
        public static ConfigValue operator ++(ConfigValue val)
        {
            val.Value++;
            if(val.Value > val.MaxValue)
            {
                val.Value = 0;
                if (val.Next != null)
                {
                    val.Next++;
                } else
                {
                    val.IsDone = true;
                }
            }
            return val;
        }
    }
}
