﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class BoardSave  {

    public int[,] SpaceData;
    public int[] GoalColors;
    public int[] ColorUses; //RGB
}
