﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utilities : MonoBehaviour {

    public static Utilities instance;
    public enum Direction { LEFT, RIGHT, UP, DOWN };
    public GameObject spacePrefab , mirrorPrefab , goalPrefab , wallPrefab , emitterPrefab;
    public ColorButton RedColorButton, BlueColorButton, GreenColorButton;
    public Color touchColor;
    public static Color TouchColor { get { return instance.touchColor; }  set { instance.touchColor = value; } }
    public static GameObject SpacePrefab { get { return instance.spacePrefab; } }
    public static GameObject MirrorPrefab { get { return instance.mirrorPrefab; } }
    public static GameObject GoalPrefab { get { return instance.goalPrefab; } }
    public static GameObject WallPrefab { get { return instance.wallPrefab; } }
    public static GameObject EmitterPrefab { get { return instance.emitterPrefab; } }
    public static BoardSpace TileSelected;
    void Start()
    {
        if (instance == null) instance = this;
        else
        {
            Debug.LogWarning("Utility script on: " + gameObject.name + " is redundant. You should remove it");
            Destroy(this);
        }
    }

    public static void RemoveUse()
    {
        if (instance.touchColor == Color.red) instance.RedColorButton.Uses--;
        if (instance.touchColor == Color.green) instance.GreenColorButton.Uses--;
        if (instance.touchColor == Color.blue) instance.BlueColorButton.Uses--;
    }

    public static void RefundUse(Color inColor)
    {
        if (inColor == Color.red) instance.RedColorButton.Uses++;
        if (inColor == Color.green) instance.GreenColorButton.Uses++;
        if (inColor == Color.blue) instance.BlueColorButton.Uses++;
    }

    public static bool CanPlace()
    {
        if (instance.touchColor == Color.red) return instance.RedColorButton.Uses > 0;
        if (instance.touchColor == Color.green) return instance.GreenColorButton.Uses > 0;
        if (instance.touchColor == Color.blue) return instance.BlueColorButton.Uses > 0;
        else return false;
    }
    public static Direction GetDirection(Vector3 vector)
    {
        if (Mathf.Abs(vector.x) > Mathf.Abs(vector.y))
        {
            return vector.x > 0 ? Direction.RIGHT : Direction.LEFT;
        } else
        {
            return vector.y > 0 ? Direction.UP : Direction.DOWN;
        }
    }

}
