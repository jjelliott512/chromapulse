﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPulse {

    public Color color;
    public Utilities.Direction Direction;
    public BoardSpace Position;


    public ColorPulse(BoardSpace space , Utilities.Direction dir , Color color)
    {
        this.color = color;
        Direction = dir;
        Position = space;
    }

    public void Tick()
    {
        if (Position == null) return;
        if(Position[Direction] != null)
        {
            Position = Position[Direction];
            Position.AddColor(this);
        } else
        {
            Position = null;
        }
    }
}
