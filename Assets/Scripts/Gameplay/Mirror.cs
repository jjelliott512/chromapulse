﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Mirror : SpaceObject {

    bool IsFlipped = false; //Default state is Top-Left to Bottom-Right
    public Image TopImage, BottomImage;
    public override void Resolve()
        {
        InitialPulses.Clear();
        if(InPulses.Count == 0)
        {
            Reset();
        }
        foreach (var pulse in InPulses)
        {
            InitialPulses.Add(new ColorPulse(this, Reflect(pulse.Direction), pulse.color));
            ColorHalf(pulse);
        }
        InPulses.Clear();
    }

    public void Flip()
    {
        IsFlipped = !IsFlipped;
        RectTransform trans = GetComponent<RectTransform>();
        trans.localScale = new Vector3(trans.localScale.x , IsFlipped ? -trans.localScale.y: trans.localScale.y , trans.localScale.z);
    }

    public override void Reset()
    {
        base.Reset();
        foreach (var image in GetComponentsInChildren<Image>())
        {
            image.color = Color.white;
        }
    }

    private Utilities.Direction Reflect(Utilities.Direction inDirection)
    {
        switch (inDirection)
        {
            case Utilities.Direction.LEFT:
                return IsFlipped ? Utilities.Direction.DOWN : Utilities.Direction.UP;
                break;
            case Utilities.Direction.RIGHT:
                return IsFlipped ? Utilities.Direction.UP : Utilities.Direction.DOWN;
                break;
            case Utilities.Direction.UP:
                return IsFlipped ? Utilities.Direction.RIGHT : Utilities.Direction.LEFT;
                break;
            case Utilities.Direction.DOWN:
                return IsFlipped ? Utilities.Direction.LEFT : Utilities.Direction.RIGHT;
                break;
            default:
                return Utilities.Direction.DOWN;
        }
    }

    private void ColorHalf(ColorPulse inPulse)
    {
        Image toColor = null;
        switch (inPulse.Direction)
        {
            case Utilities.Direction.LEFT:
                toColor = BottomImage;
                break;
            case Utilities.Direction.RIGHT:
                toColor = TopImage;
                break;
            case Utilities.Direction.UP:
                toColor = !IsFlipped ? TopImage : BottomImage;
                break;
            case Utilities.Direction.DOWN:
                toColor = IsFlipped ? TopImage : BottomImage;
                break;
        }
        if (toColor != null)
        toColor.color = inPulse.color;
    }
}
