﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BoardSpace : MonoBehaviour {

    public enum SpaceType { Empty , Mirror , Goal , Wall , Emitter}
    public SpaceType Type;
    private SpaceType PrevType;
    protected List<ColorPulse> InPulses;
    public List<ColorPulse> InitialPulses;
    protected GameBoard Board;
    protected int x, y;
    
    Image Panel;

    
    public void Init(GameBoard inBoard , int x , int y)
    {
        Board = inBoard;
        InitialPulses = new List<ColorPulse>();
        this.x = x;
        this.y = y;
        
        InPulses = new List<ColorPulse>();
    }
    void ResetColors()
    {
        InPulses.Clear();
    }

    public void AddColor(ColorPulse InPulse)
    {
        InPulses.Add(InPulse);
    }

    public BoardSpace this[Utilities.Direction inDirection]
    {
        get {
            if (HasNeighbor(inDirection))
            {
                return GetNeighbor(inDirection);
            } else
            {
                return null;
            }
            }
            
    }


    public bool HasNeighbor(Utilities.Direction inDirection)
    {
        switch ((int)inDirection)
        {
            case 0:
                return x - 1 >= 0;
            case 1:
                return x + 1 < Board.Width;
            case 2:
                return y - 1 >= 0;
            case 3:
                return y + 1 < Board.Height;
            default:
                return false;

        }
    }

    private BoardSpace GetNeighbor(Utilities.Direction inDirection)
    {
        switch (inDirection)
        {
            case Utilities.Direction.LEFT:
                return Board[x-1,y];
            case Utilities.Direction.RIGHT:
                return Board[x+1,y];
            case Utilities.Direction.UP:
                return Board[x, y - 1];
            case Utilities.Direction.DOWN:
                return Board[x, y + 1];
            default:
                return null;
        }
    }

    public void Start()
    {
        PrevType = Type;
    }

    public virtual void FixedUpdate()
    {
        if(Type != PrevType)
        {
            switch (Type)
            {
                case SpaceType.Empty:
                    Board[x, y] = gameObject.AddComponent<BoardSpace>();
                    GetComponent<Image>().sprite = Utilities.SpacePrefab.GetComponent<Image>().sprite;
                    Destroy(this);
                    break;
                case SpaceType.Mirror:
                    Board[x, y] = gameObject.AddComponent<Mirror>();
                    GetComponent<Image>().sprite = Utilities.MirrorPrefab.GetComponent<Image>().sprite;
                    Destroy(this);
                    break;
                case SpaceType.Goal:
                    Board[x, y] = gameObject.AddComponent<Goal>();
                    GetComponent<Image>().sprite = Utilities.GoalPrefab.GetComponent<Image>().sprite;
                    Destroy(this);
                    break;
                case SpaceType.Wall:
                    Board[x, y] = gameObject.AddComponent<Wall>();
                    GetComponent<Image>().sprite = Utilities.WallPrefab.GetComponent<Image>().sprite;
                    Destroy(this);
                    break;
                case SpaceType.Emitter:
                    Board[x, y] = gameObject.AddComponent<Emitter>();
                    GetComponent<Image>().sprite = Utilities.EmitterPrefab.GetComponent<Image>().sprite;
                    Destroy(this);
                    break;
            }
            PrevType = Type;
        }
    }
    

    public virtual void Tick()
    {
        foreach(var p in InitialPulses)
        {
            p.Tick();
        }
    }

    public virtual void Resolve()
    {

        if(InPulses.Count >= 1)
        {
            print(InPulses.Count);
            Color totalColor = Color.black;
            foreach(var pulse in InPulses)
            {
                totalColor += pulse.color;
            }
            totalColor.r = totalColor.r > 1 ? 1 : totalColor.r;
            totalColor.b = totalColor.b > 1 ? 1 : totalColor.b;
            totalColor.g = totalColor.g > 1 ? 1 : totalColor.g;
            totalColor.a = totalColor.a > 1 ? 1 : totalColor.a;
            
            for(int i = 0; i <InPulses.Count; i++)
            {
                ColorPulse p = InPulses[i];
                InitialPulses.Add(new ColorPulse(this, p.Direction, totalColor));
            }
            GetComponent<Image>().color = InitialPulses[0].color;
            InPulses.Clear();
        } else
        {
            Reset();
        }
    }

    public virtual void Reset()
    {
        InPulses.Clear();
        GetComponent<Image>().color = Color.white;
        InitialPulses.Clear();
        Utilities.TileSelected = null;
    }
    
    
}
