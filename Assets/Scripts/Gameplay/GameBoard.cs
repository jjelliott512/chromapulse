﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class GameBoard : MonoBehaviour {

    public BoardSpace[,] Spaces;
    public int Width , Height;
    public List<Mirror> Mirrors;
    public List<Emitter> Emitters;
    public const int CellSize = 100;
    public float StepSpeed;
    private bool IsRunning;
    public GameObject BoardClearedScreen;
    public UnityEvent BoardCleared;
    public int[] Charges;
    int GoalCount = 0;

    private bool Cleared { get
        {
            bool Cleared = true;
            foreach (var g in GetComponentsInChildren<Goal>()) Cleared = Cleared && g.Cleared;
            return Cleared;
        } }

    public int MaxTestSteps = 50;

    int[,] SpaceType = { { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } , { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 2, 0, 0, 0 } , { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } , { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } , { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } , { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } , { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } };
    public void Run()
    {
        IsRunning = true;
        StartCoroutine(Tick());
    }

    public void Stop()
    {
        IsRunning = false;
        foreach (var space in Spaces)
        {
            space.Reset();
        }
        Utilities.instance.RedColorButton.Uses = Charges[0];
        Utilities.instance.GreenColorButton.Uses = Charges[1];
        Utilities.instance.BlueColorButton.Uses = Charges[2];
    }

    private IEnumerator Tick()
    {
        while (IsRunning)
        {
            foreach(var space in Spaces)
            {
                space.Tick();
            }
            foreach (var space in Spaces)
            {
                space.Resolve();
            }
            if(Cleared)
            {
                print("Board is cleared");
                BoardCleared.Invoke();
            }
            yield return new WaitForSeconds(StepSpeed); 
        }
    }

    public BoardSpace this[int x , int y]
    {
        get { if (x < Width && y < Height && x >=0 && y>=0) return Spaces[x, y];
            else return null;
        }

        set
        {
            if (x < Width && y < Height && x >= 0 && y >= 0) Spaces[x, y] = value;
        }
    }

    [ExecuteInEditMode]
    public void Init (BoardSave data)
    {
        GoalCount = 0;
        if (Spaces != null)
        {
            foreach(var s in Spaces)
            {
                Destroy(s.gameObject);
            }
            if (Mirrors != null) Mirrors.Clear();
            if (Emitters != null) Emitters.Clear();
        }
        Spaces = new BoardSpace[Width, Height];
        Charges = new int[3];
        Emitters = new List<Emitter>();
        Mirrors = new List<Mirror>();

        for (int j = 0; j < Width; j++)
        {
            for (int i = 0; i < Height; i++)
            {
                switch (data.SpaceData[i, j]){
                    case 0:
                        Spaces[i, j] = Instantiate(Utilities.SpacePrefab).GetComponent<BoardSpace>();
                        break;
                    case 1:
                        Spaces[i, j] = Instantiate(Utilities.MirrorPrefab).GetComponent<BoardSpace>();
                        Mirrors.Add(Spaces[i, j] as Mirror);
                        break;
                    case 2:
                        Spaces[i, j] = Instantiate(Utilities.GoalPrefab).GetComponent<BoardSpace>();
                        var g = Spaces[i, j] as Goal;
                        Color color = Color.red; //Defaulting just in case
                        switch (data.GoalColors[GoalCount]){

                            case 0:
                                color = Color.red;
                                break;
                            case 1:
                                color = Color.blue;
                                break;
                            case 2:
                                color = Color.green;
                                break;
                            case 3:
                                color = Color.magenta;
                                break; 
                            case 4:
                                color = Color.yellow;
                                break;
                            case 5:
                                color = Color.cyan;
                                break;
                            case 6:
                                color = Color.white;
                                break;
                        }
                        g.TargetColor = color;
                        g.Init();
                        GoalCount++;
                        break;
                    case 3:
                        Spaces[i, j] = Instantiate(Utilities.WallPrefab).GetComponent<BoardSpace>();
                        break;
                    case 4:
                        Spaces[i, j] = Instantiate(Utilities.EmitterPrefab).GetComponent<BoardSpace>();
                        Emitters.Add(Spaces[i, j] as Emitter);
                        print(Spaces[i, j]);
                        break;


                }

                Spaces[i, j].Init(this, i, j);
                Spaces[i, j].gameObject.transform.SetParent(transform, false);
            }

        }

       Utilities.instance.RedColorButton.Uses = Charges[0] = data.ColorUses[0];
       Utilities.instance.GreenColorButton.Uses = Charges[1] = data.ColorUses[1];
       Utilities.instance.BlueColorButton.Uses = Charges[2] = data.ColorUses[2];
        //Set the size of the transform

        GetComponent<RectTransform>().sizeDelta = new Vector2(CellSize * Width, CellSize * Height);
        GetComponent<GridLayoutGroup>().cellSize = new Vector2(CellSize, CellSize);
    }

    public void Reset()
    {
        if (Spaces != null)
        {
            foreach (var b in Spaces)
            {
                Destroy(b.gameObject);
                Destroy(b);
            }
            Spaces = null;
        }
        if(Charges == null) { Charges = new int[] { 0, 0, 0 };}
        Utilities.instance.RedColorButton.Uses = Charges[0];
        Utilities.instance.GreenColorButton.Uses = Charges[1];
        Utilities.instance.BlueColorButton.Uses = Charges[2];
        BoardClearedScreen.SetActive(false);
    }
    void Start()
    {
        
    }

    public void OnCleared()
    {
        IsRunning = false;
        BoardClearedScreen.SetActive(true);
    }

    public bool SimulateConfiguration()
    {

        for(int i =0; i < MaxTestSteps; i++)
        {
            foreach (var space in Spaces)
            {
                space.Tick();
            }
            foreach (var space in Spaces)
            {
                space.Resolve();
            }
        }

        return Cleared;
    }
}
