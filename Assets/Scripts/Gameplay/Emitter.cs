﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Emitter : BoardSpace {

    bool ActiveTouch = false;
    public LineRenderer LRenderer;
    Vector3 StartPos;
    public GameObject Indicator;

    public void Touched()
    {
        if (!Utilities.CanPlace()) return;
        if(InitialPulses.Count > 0)
        {
            Utilities.RefundUse(InitialPulses[0].color);
        }
        transform.parent.parent.gameObject.GetComponent<GraphicRaycaster>().enabled = false;
        ActiveTouch = true;
        Utilities.TileSelected = this;
        LRenderer.startColor = Utilities.TouchColor;
        LRenderer.enabled = true;
        GetComponent<Image>().color = Utilities.TouchColor;
        StartPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        LRenderer.SetPosition(0, StartPos + new Vector3(0, 0, 80));
    }

    private void Held()
    {
        LRenderer.SetPosition(1, Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0, 0, 80));
    }

    private void Released()
    {
        ActiveTouch = false;
        //LRenderer.enabled = false;
        InitialPulses = new List<ColorPulse>();
        InitialPulses.Add(new ColorPulse(this, Utilities.GetDirection(Camera.main.ScreenToWorldPoint(Input.mousePosition) - StartPos), Utilities.TouchColor));
        Utilities.TileSelected = null;
        transform.parent.parent.gameObject.GetComponent<GraphicRaycaster>().enabled = true;
        Utilities.RemoveUse();
    }


    // Update is called once per frame
    void Update () {
        if (ActiveTouch)
        {
            if (Input.GetMouseButtonDown(0)/*Input.GetTouch(0).phase == TouchPhase.Ended*/)
            {
                Released();
            }
            else
            {
                Held();
            }
        }
    }

    public override void Reset()
    {
        base.Reset();
        ActiveTouch = false;
        LRenderer.enabled = false;
    }
}
