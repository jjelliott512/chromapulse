﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Goal : BoardSpace {

    public Color TargetColor;
    public Image ColoredSprite;
    protected bool isResolved = false;
    public bool Cleared { get { return isResolved; } }

    public void Init()
    {
        if (ColoredSprite)
        {
            ColoredSprite.color = TargetColor;
        } else
        {
            GetComponent<Image>().color = TargetColor;
        }
    }
    public override void Resolve()
    {
        InitialPulses.Clear();
        foreach(var pulse in InPulses)
        {
            if(pulse.color == TargetColor)
            {
                GetComponent<Image>().color = TargetColor;
                isResolved = true;
                InitialPulses.Add(pulse);
            }
        }
    }

    public override void Reset()
    {
        base.Reset();
        Init();
    }
}
