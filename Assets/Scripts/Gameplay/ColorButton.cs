﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ColorButton : MonoBehaviour {

    [SerializeField]
    private Color MyColor;
    public int Uses;
    public Text UsesIndicator;
    
    void Start()
    {
        GetComponent<Image>().color = MyColor;
    }

    public void Clicked()
    {
        if (Uses > 0 ){
            Utilities.TouchColor = MyColor;
        }
        
    }
    void Update()
    {
        if(Uses <= 0)
        {
            UsesIndicator.text = "0";
            GetComponent<Image>().color = Color.grey;
        } else
        {
            UsesIndicator.text = Uses.ToString();
            GetComponent<Image>().color = MyColor;
        }
    }
}
